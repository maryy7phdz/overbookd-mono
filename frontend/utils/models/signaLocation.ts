export interface SignaLocation {
  id: number;
  name: string;
}

export interface SignaLocationCreate {
  name: string;
}
