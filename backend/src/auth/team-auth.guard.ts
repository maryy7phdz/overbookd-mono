import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtPayload } from './entities/JwtPayload.entity';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    const { user } = context.switchToHttp().getRequest();
    const payload = new JwtPayload(user);
    return payload.isAdmin() || payload.hasOneOfRequiredRoles(roles);
  }
}
